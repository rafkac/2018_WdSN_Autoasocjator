#include "autoasocjator.h"
#include "ui_autoasocjator.h"

#include <QPainter>
#include <QPaintEvent>

/**
 * RafKac
 * 26 listopada 2018
 * 4 grudnia 2018
 *
 * Zadanie 2 - autoasocjator graficzny.
 *
 * Pomysł:
 * - mam trzy obrazki (albo k), 50x50 px,
 * - tworzę 2500 perceptronów (perceptrony te są dla wszystkich obrazków, nie dla każdego),
 * - dla każdego perceptrona:
 *   - wczytuję obrazek (najpierw niezaszumiony),
 *   - interpretuję ten perceptron jako i-ty piksel
 *   - liczę wartość i-tego piksela (suma to jedno, drugą część alternatywy otrzymuję z porównania z obrazkami bazowymi),
 *   - i jak w pierwszym zadaniu reszta jest uzależniona od wartości Error.
 *
 * Plan:
 * 1) najpierw uczę i sprawdzam na niezaszumionych obrazkach,       O v
 * 2) sprawdzam na zaszumionych obrazkach                           O v
 * 3) uczę i sprawdzam na zaszumionych                              O v
 * 4) zapis wag do pliku                                            O
 * 5) odczyt wag z pliku                                            O
 *
 * Oczywiście perceptrony są z kieszonką i zapadką.
 *
 * Zmienne globalne:
 *  - przyciskNauka,
 *  - przyciskLosuj,
 *  - przyciskExit,
 *  - przyciskWczytajWagi,
 *  - przyciskZapiszWagi,
 *  - przyciskJakoRoboczy
 *
 *  - QVector<Perceptron> listaPerceptronow,
 *  - QVector<Przyklad> listaPrzykladow       (są to obrazy zmienione na przykłady, jeszcze trzeba napisać metodę, która robi tą konwersję)
 *
 * Metody:
 *   void paintEvent(QPaintEvent *);
 *   void mousePressEvent(QMouseEvent *event);
 *   void putPixel(int x, int y, QRgb color, QImage *obrazek);
 *   void zamalujSuperPixel(int x, int y);
 *   void zamalujSuperPixel(int x, int y, QRgb kolor);
 *   void rysujRysunek(Przyklad p);
 *   void rysujRysunekPrawy(Przyklad p);
 *   void sprawdzPrzyklad(Przyklad &p);
 *   Przyklad zaburzPrzyklad(Przyklad p);
 *   void czysc();
 *
 *
 * Metody do obsługi przycisków:
 *   void uczPerceptrony();
 *   void wczytajPrzyklad();
 *   void losujPrzyklad();
 *   void zapiszWagi();
 *   void wczytajWagi();
 *   void potraktujJakoRoboczy();
 *
 *
 *
 * UWAGI:
 *  1. Przykłady dodaję w konstruktorze
 *  2. Perceptrony tworzę w kostruktorze.
 *  3. Obrazki są rzutowane na listę pikseli już w konstruktorze, dalej już są dla nas tylko jako tablice bitów {0 i 1},
 *  nie korzystamy więcej z tego, że są to obrazki.
 *  4. Obraz prawy (czyli wynikowy) ma swoją metodę rysującą na nim superpiksele. Możnaby to zrobić jedną metodą,
 *  ale wtedy byłoby wskazane dodanie dodatkowej zmiennej globalnej, która by pilnowała, na którym obrazku rysujemy
 *  w danym momencie. Na razie zostawiamy w takiej postaci.
 *
 */

Autoasocjator::Autoasocjator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Autoasocjator){
    ui->setupUi(this);
    srand(time(NULL));
    img1 = QImage("img1.png");
    img2 = QImage("img2.png");
    img3 = QImage("img3.png");
    imgPrawy = new QImage(500,500, QImage::Format_RGB32);
    imgLewy = new QImage(500, 500, QImage::Format_RGB32);

    imgPrawy->fill(Qt::white);
    imgLewy->fill(Qt::white);

    czyJestTrybNauki = true;

    connect(ui->przyciskExit, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->przyciskLosuj, SIGNAL(clicked()), this, SLOT(losujPrzyklad()));
    connect(ui->przyciskNauka, SIGNAL(clicked()), this, SLOT(uczPerceptrony()));
    connect(ui->przyciskWczytajWagi, SIGNAL(clicked()), this, SLOT(wczytajWagi()));
    connect(ui->przyciskZapiszWagi, SIGNAL(clicked()), this, SLOT(zapiszWagi()));
    connect(ui->przyciskJakoRoboczy, SIGNAL(clicked()), this, SLOT(potraktujJakoRoboczy()) );
    connect(ui->przyciskWczytaj, SIGNAL(clicked()), this, SLOT(wczytajPrzyklad()));

    listaPerceptronow = QVector<Perceptron>();
    listaPrzykladow = QVector<Przyklad>();

    Przyklad p1 = Przyklad(1);
    p1.wczytajObraz(img1);
    p1.setJakaJestCyfra(1);
    listaPrzykladow.push_back(p1);

    Przyklad p2 = Przyklad(2);
    p2.setJakaJestCyfra(2);
    p2.wczytajObraz(img2);
    listaPrzykladow.push_back(p2);

    Przyklad p3 = Przyklad(3);
    p3.setJakaJestCyfra(3);
    p3.wczytajObraz(img3);
    listaPrzykladow.push_back(p3);

 /*   qDebug("Testowe wypisanie przykładów:");
    for(int i = 0; i < listaPrzykladow.size(); i++ ){
        qDebug("Przykład %d", i);
        Przyklad pp = listaPrzykladow[i];
        if(pp.getLista().empty()){
            qDebug("Pusta lista");
        }
        pp.wypisz();
    }*/
    qDebug("Liczymy jedynki w przykładach:");
    for(int i = 0; i < listaPrzykladow.size(); i++){
        int jedynka = 0;
        Przyklad p = listaPrzykladow.at(i);
        for(int j = 0; j < 2500; j++){
            if(p.getMiejsce(j) == 1){
                jedynka++;
            }
        }
        qDebug("W obrazku %d mamy %d jedynek i %d zer /n\n" , i, jedynka, 2500-jedynka);
    }

}


Autoasocjator::~Autoasocjator(){
    delete ui;
}

void Autoasocjator::paintEvent(QPaintEvent *){
    QPainter p(this);
    p.drawImage(0,0, *imgLewy);
    p.drawImage(510, 0, *imgPrawy);
}

/**
 * Metoda ta przechwytuje kliknięca myszy na lewym ekranie. Jeśli dany superpiksel był biały, to maluje go na czarno,
 * jeśli zaś był czarny, to na biało.
 * Działa tylko dla lewego obrazka - roboczego - w innym miejscu zwróci błąd "rysujemy poza obrazkiem".
 */
void Autoasocjator::mousePressEvent(QMouseEvent *event){
    if(czyJestTrybNauki){
       qDebug("mousePressEvent - Jesteśmy w trybie nauki");
    }
    else{
        zamalujSuperPixel(event->x(), event->y());
    }
}

/**
 * Poniższa metoda najpierw sprawdza, czy nie jesteśmy poza obszarem obrazka,
 * a następnie zapala pojedyńczy superpixel.
 */
void Autoasocjator::putPixel(int x, int y, QRgb color, QImage *obrazek){
    if(x >= 0 && x < obrazek->width() && y >= 0 && y < obrazek->height()){
        obrazek->setPixel(x, y, color);
    }
    else{
        qDebug("Jesteśmy poza obrazkiem.");
    }
    update();
}

/**
 * Metoda ta zamalowuje superpiksel (10 na 10 pikseli).
 *
 * Konkretnie - pobieramy kolor z klikniętego piksela, a następnie zamalowujemy piksel na kolor przeciwny.
 * Działa tylko dla lewego obrazka.
 */
void Autoasocjator::zamalujSuperPixel(int x, int y){
    QRgb color = imgLewy->pixel(x,y);
    QRgb colorBialy = qRgb(255,255,255);
    // poniższe działanie robimy po to, żebyśmy mieli część całkowitą tego punktu, czyli lewy górny róg
    int xG = ( x/10 )* 10;
    int yG = ( y/10 )* 10;
    if(color == colorBialy){
        color = qRgb(0,0,0);
    }
    else{
        color = qRgb(255,255,255);
    }
    //    qDebug("Współrzędne klikniecia: (%d, %d), współrzędne rogu: (%d, %d)", x, y, xG, yG);
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
            putPixel(xG + i, yG + j, color, imgLewy);
        }
    }
    update();
}

/**
 * Inna wersja wczesniejszej metody, zamalowuje superpiksel na podany kolor.
 *
 * Dostaje jako parametry współrzędne lewego górnego wierzchołka superpiksela,
 * który chcemy zamalować.
 *
 * Zamalowujemy w obrazku odpowiednim do współrzędnych, jakie dostaniemy jako paramtery.
 *
 * Po każdym dodaniu superPixela nadpisujemy wartość odpowiedniej zmiennej obrazowej.
 *
 * przekazujemy wskaźnik do funkcji, bo innaczej może nam zabraknąć pamięci
 */
void Autoasocjator::zamalujSuperPixel(int x, int y, QRgb kolor){
//    qDebug("zamalujSuperPixel: (%d, %d)", x, y);
    QImage *obraz;
    if(x < 500){
        obraz = imgLewy;
    }
    else{
        obraz = imgPrawy;
        x = x - 510;
    }
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
            putPixel(x + i, y + j, kolor, obraz);
        }
    }
/*    if(y >= 100 && x >= 100 && y < 200){
        qDebug("y >= 100, zamalujSuperPiksel()");
    }
    if(x < 500){
        *imgLewy = *obraz;
    }
    else{
        *imgPrawy = *obraz;
    }*/

    update();
}

/**
 * Metoda dostaje na wejściu Przykład. Następnie interpretuje go jako rysunek.
 * Każdą kolejną cyfrę interpretujemy jako zapalenie (1 - czarny)
 * lub zgaszenie (0 - biały) superpiksela.
 *
 * Metoda jest uniwersalna - to, na którym rysunku rysujemy, zależy od współrzędnych.
 *
 * qRgb(0, 0, 0)       - kolor czarny
 * qRgb(255, 255, 255) - kolor biały
 */
void Autoasocjator::rysujRysunek(Przyklad p){
//    qDebug("rysujRysunek");
    int x = 0;
    int y = 0;
    int j = 0;
    for(int i = 0; i < p.dlugoscPrzykladu(); i++){
        // liczymy numery wiersza i kolumny wierzchołka superpiksela
        x = (i % 50) * 10;
        if(x == 0 && i>0){
            j++;
        }
        y = j * 10;
        if(p.getMiejsce(i) == 1){
            // zapalamy superpiksel - na czarno
            zamalujSuperPixel(x, y, qRgb(0,0,0));
//            qDebug("Zapalamy superpixel (%d, %d)", x,y);
        }
        else{
            // gasimy superpiksel - na biało
            zamalujSuperPixel(x, y, qRgb(255,255,255));
 //           qDebug("gasimy superpixel (%d, %d)", x,y);
        }
    }
    update();
    qDebug("koniec rysujRysunek");
}

/**
 * Metoda ta jest taka sama, jak powyższa, ale pilnuje rysowania na prawym rysunku, przez co
 * musimy mieć przesunięcie o 510 pikseli w prawo.
 *
 * Dodana:
 * 4 grudnia 2018
 */
void Autoasocjator::rysujRysunekPrawy(Przyklad p){
    qDebug("RysujRysunekPrawy");
    int j = 0;
    int x = 0;
    int y = 0;
    for(int i = 0; i < p.dlugoscPrzykladu(); i++){
        // liczymy numery wiersza i kolumny wierzchołka superpiksela
        x = (i % 50) * 10;
        if(x == 0 && i>0){
            j++;
        }
        y = j * 10;
        if(p.getMiejsce(i) == 1){
            // zapalamy superpiksel - na czarno
            zamalujSuperPixel(x + 510, y, qRgb(0,0,0));
    //            qDebug("Zapalamy superpixel (%d, %d)", x,y);
        }
        else{
            // gasimy superpiksel - na biało
            zamalujSuperPixel(x + 510, y, qRgb(255,255,255));
//            qDebug("gasimy superpixel (%d, %d)", x,y);
        }
    }
    update();
    qDebug("koniec rysujRysunekPrawy");
}

/**
 * Metoda ta jest wywoływana po nauszeniu naszej sieci.
 *
 * Przepuszcza dany przykład przez naszą sieć neuronową.
 *
 * Jeżeli i-ty perceptron zwróci nam coś większego od zera, to zapalamy i-ty piksel
 * (czyli musimy dokonać konwersji, przeliczenia współrzędnych).
 *
 * Pamiętamy, że i-ty perceptron pilnuje nam tylko jednego piksela, i-tego
 * (jeśli piksele interpretujemy jako listę).
 *
 * Ponadto zapalamy piksele na prawym rysunku, dlatego to przesunięcie o 510.
 */
void Autoasocjator::sprawdzPrzyklad(Przyklad &p){
    qDebug("Wywołujemy metodę sprawdzPrzyklad().");
    if(czyJestTrybNauki){
        qDebug("Jesteśmy w trybie nauki.");
    }
    else{
//        qDebug("Metoda sprawdzPrzyklad() - początek.");
//        qDebug("Sprawdzany przykład:");
//        p.wypisz();
        int j = 0;
        int x = 0;
        int y = 0;
//        rysujRysunek(p);
//        p.wypisz();
        for(int i = 0; i < listaPerceptronow.size(); i++){
            Perceptron per = listaPerceptronow.at(i);
            per.wszystkieOperacje2(p);
            x = (i % 50) * 10;
            if(x == 0 && i > 0){
                j++;
//                fprintf(stderr, "\n");
            }
            y = j * 10;
            if(per.getWynikDzialaniaSieci() > 0){
//                qDebug("Malujemy na czarno.");
                zamalujSuperPixel(x + 510, y, qRgb(0, 0, 0));
            }
            else{
                zamalujSuperPixel(x + 510, y, qRgb(255, 255, 255));
            }
//            fprintf(stderr, "%c", per.getWynikDzialaniaSieci() == 1 ? 'X' : '-');

        }
    }
    qDebug("SprawdzPrzyklad() - koniec. \n");
    update();
}





/**
 * Metoda służy do zaburzenia otrzymanego na wejściu przykładu. Zwraca zaburzony przykład.
 *
 * Działanie:
 *  - losujemy liczbę k z przedziału 10-20 (będzie ona oznaczała ilość pikseli,
 * jakie powinniśmy zaburzyć).
 *  - k-razy losujemy współrzędne x i y z zakresu (0, 50) - będą to (po przemnożeniu przez 10)
 * współrzędne lewego górnego wierzchołka superpiksela.
 *  - w wylosowanym superpikselu pobieramy kolor,
 *  - w wylosowanym superpikselu zamieniamy kolor na przeciwny.
 *
 * Ewentualnie możemy wykorzystać fakt, że lista superpikseli jest dla nas listą cyfr {0, 1}.
 * Bazując na tym po prostu losujemy liczbę z przedziału (0, 2500), czytamy, co tam jest
 * i podmieniamy na przeciwne.
 *
 * Ukończona - 4 grudnia 2018
 */
Przyklad Autoasocjator::zaburzPrzyklad(Przyklad p){
    int ileZaburzonych = 10 + rand() % 20;
//    qDebug("zaburzamy: %d, długość Przykładu: %d", ileZaburzonych, p.dlugoscPrzykladu());
    for(int i = 0; i < ileZaburzonych; i++){
        int ktorySuperPixel = rand() % 2500;
        int wartosc = p.getMiejsce(ktorySuperPixel);
//        qDebug("ZaburzPiksel, zaburzamy: %d, miejsce: %d, było: %d.", ileZaburzonych, ktorySuperPixel, wartosc);
        if(wartosc == 1){
            wartosc = -1;
        }
        else{
            wartosc = 1;
        }
//        qDebug("ZaburzPiksel: Nowa wartość: %d", wartosc);
        p.setMiejse(ktorySuperPixel, wartosc);
    }
//    qDebug("Zakończono zaburzanie przykładu,");
    return p;
}

/**
 * Metoda ta odpowiada za wypełnienie kolorem białym obu obszarów roboczych.
 * Nie jestem pewien, czy bedzie potrzebna.
 * 3 grudnia 2018
 */
void Autoasocjator::czysc(){
    imgLewy->fill(Qt::white);
    imgPrawy->fill(Qt::white);
    update();
}

/**
 * Metoda odpowiada za cały proces nauki perceptronów.
 *
 * 1) czy listaPerceptronów jest pusta?
 * 2)   jeśli tak - stwarzamy 2500 perceptronów, każdy z kolejnym numerem
 * 3)   jeśli nie - przejdź do 4)
 * 4) dla każego perceptrona:
 * 5)    w zakresie od 1 do n {np n = 100}
 * 6)         Losuj przykład z listyPrzykladow
 * 7)         Jesli uczymy na zaburzonych, to zaburz,
 * 8)         liczymy wartość wyjściową dla tego perceptronu {sigma > 0 => 1 ELSE => -1}
 * 9)         czy wartość wyliczona jest taka sama jak to, co było w tym pikselu na starcie?
 * 10)            jeśli tak, to 1
 * 11)            jeśli nie, to -1
 * 12)        liczymy ERR
 * 13)        jeśli ERR <> 0, to uaktualniamy wagi (z zapadką etc)
 * 14)        przejdź do 5)
 * 15)   perceptron jest nauczony, możemy uczyć kolejny - przechodzimy do 4)
 * 16) perceptrony zostały nauczone - możemy testować.
 *
 * Ponadto chcemy, żeby to były perceptrony z kieszonką i zapadką.
 * 4 grudnia 2018
 * 5 grudnia 2018 - mamy sytuację: uczymy na niezaburzonych rysunkach, sprawdzamy na zaburzonych,
 *  wciąż olbrzymia większość perceptronów ma 0 czas życia rekordzisty, a te z niezerowym poprawnie
 *  rozpoznają co najwyżej dwa.
 *
 * 5 lutego 2019
 * jestem głupi - uczylem perceptron na kopii elementu z listy, a następnie nie nadpisywałem odpowiedniego
 * na liście
 */
void Autoasocjator::uczPerceptrony(){
    int iloscIteracjiNauki = 1500;
    int rozpoznalTrzyObrazki = 0;
    int rozpoznalDwaObrazki = 0;
    int rozpoznalJedenObrazek = 0;
    qDebug("Rozpoczynamy naukę perceptronów.");
    if(listaPerceptronow.isEmpty()){
        qDebug("uczPerceptrony() - Tworzymy perceptrony.");
        for(int i = 0; i < 2500; i++){
            Perceptron p = Perceptron(i);
            listaPerceptronow.push_back(p);
        }
    }
//    listaPerceptronow[0].wypiszTabliceWag();
    for(int i = 0; i < listaPerceptronow.size(); i++){
        qDebug("Uczymy %d perceptron: ", i);
        int numerPrzykladu = 0;
        int aktualizacjeWag = 0;
//        int zapadka = 0;
//        int maxZapadka = 0;
        int czasZyciaWag = 0;
        int czasZyciaRekordzisty = 0;
        Perceptron perceptronIty = listaPerceptronow[i];
        double thetaKieszonka = perceptronIty.getTheta();
        QVector<double> wagiKieszonka = perceptronIty.getTablicaWag();
        for(int j = 0; j < iloscIteracjiNauki; j++){
            numerPrzykladu = rand() % 3;
            Przyklad p = listaPrzykladow.at(numerPrzykladu);
//            rysujRysunek(p);
            p = zaburzPrzyklad(p);
            perceptronIty.wszystkieOperacje2(p);
            if(perceptronIty.getErr() == 0){
//                qDebug("błąd = 0, dobrze sklasyfikowaliśmy");
                czasZyciaWag++;
            }
            else{
//               zapadka = perceptronIty.liczKlasyfikowanePrzyklady(listaPrzykladow);
                qDebug("\n czasŻyciaWag: %d,czasRekordzisty: %d, thetaKieszonka: %f, iteracja: %d, przyklad: %d", czasZyciaWag, czasZyciaRekordzisty, thetaKieszonka, j, numerPrzykladu);
                if(czasZyciaWag >= czasZyciaRekordzisty /*&& zapadka >= maxZapadka*/){
                    qDebug("Podmieniamy wartość wag w kieszonce.");
                    czasZyciaRekordzisty = czasZyciaWag;
                    wagiKieszonka = perceptronIty.getTablicaWag();
//                    maxZapadka = zapadka;
                    qDebug("thetaKieszonka: %f, getTheta(): %f", thetaKieszonka, perceptronIty.getTheta());
                    thetaKieszonka = perceptronIty.getTheta();
                    qDebug("thetaKieszonka: %f, getTheta(): %f", thetaKieszonka, perceptronIty.getTheta());
                }
                aktualizacjeWag++;
                czasZyciaWag = 0;
                perceptronIty.uaktualnijWagi(p);
            }

        }
        // ostatnia aktualizacja, sprawdzamy, czy ostatni nie jest najlepszy możliwy
//                        zapadka = perceptronIty.liczKlasyfikowanePrzyklady(listaPrzykladow);
        if(czasZyciaWag >= czasZyciaRekordzisty /*&& zapadka >= maxZapadka*/){
//                            qDebug("Podmieniamy wartość wag w kieszonce.");
                            czasZyciaRekordzisty = czasZyciaWag;
                            wagiKieszonka = perceptronIty.getTablicaWag();
//                            maxZapadka = zapadka;
                            thetaKieszonka = perceptronIty.getTheta();
                            aktualizacjeWag++;
                        }
        qDebug("Ilość aktualizacji: %d, czasŻyciaW: %d, czasZyciaR: %d, theta: %f", aktualizacjeWag, czasZyciaWag, czasZyciaRekordzisty, thetaKieszonka);
        qDebug("dobrze klasyfikowane: %d", perceptronIty.liczKlasyfikowanePrzyklady(listaPrzykladow));
        perceptronIty.setTablicaWag(wagiKieszonka);
        perceptronIty.setTheta(thetaKieszonka);
 /*       if(maxZapadka == 3){
            rozpoznalTrzyObrazki++;
        }
        if(maxZapadka == 2){
            rozpoznalDwaObrazki++;
        }
        if(maxZapadka == 1){
            rozpoznalJedenObrazek++;
        }*/
//        qDebug("Max zapadka: %d, czas życia rekordzisty: %d dla perceptrona: %d.", maxZapadka, czasZyciaRekordzisty, perceptronIty.getN());
        listaPerceptronow[i] = perceptronIty;
    }


    qDebug("%d - trzy obrazki, %d - dwa obrazki, %d - jeden obrazek.", rozpoznalTrzyObrazki, rozpoznalDwaObrazki, rozpoznalJedenObrazek);
    czyJestTrybNauki = false;
    qDebug("uczPerceptrony() - koniec");
//    listaPerceptronow[0].wypiszTabliceWag();
}

/**
 * Poniższa metoda odpowiada za wczytanie wyklikanego przykładu. Następnie przepuszczamy go
 * przez sieć neuronową w celu uzyskanie wyniku - który będzie wyświetlonyna prawym obrazku.
 * - wczytujemy prawy obrazek,
 * - robimy z niego nowy przykład,
 * - przepuszczamy przykład przez siec nauronową,
 * - patrzymy, co nam wyszło
 *
 * Uwaga:
 * Pamiętamy o tym, że czytamy tylko lewy górny wierzchołek superpiksela.
 *
 * 4 grudnia 2018
 *
 * 5 lutego 2019
 * poniżej był dość istotny błąd - mianowicie w przypadku koloru białego dodawałem do listy zero, a powinienem
 * dodawać -1.
 * Po poprawieniu tego błędu program bardzo ładnie działa.
 *
 */
/*
void Autoasocjator::wczytajPrzyklad(){
    if(czyJestTrybNauki){
       qDebug("Tryb nauki");
       return;
    }
    else{
        qDebug("WczytajPrzyklad() - zaczynamy.");
        int wysokosc = imgLewy->height();
        int szerokosc = imgLewy->width();
        Przyklad *p = new Przyklad(-1);
        for(int kolumna = 0; kolumna < wysokosc; kolumna = kolumna + 10){
            for(int wiersz = 0; wiersz < szerokosc; wiersz = wiersz + 10){
                QRgb kolor = imgLewy->pixel(wiersz, kolumna);
                if(kolor == qRgb(255,255,255)){
//                    qDebug("Kolor biały");
                    p->dodajDoListy(-1);
                }
                if(kolor == qRgb(0,0,0)){
//                    qDebug("Kolor czarny");
                    p->dodajDoListy(1);
                }
            }
        }
        qDebug("Przeczytaliśmy caly obrazek.");
        sprawdzPrzyklad(*p);
    }
    qDebug("Koniec metody wczytajPrzyklad().");
}
*/

void Autoasocjator::wczytajPrzyklad(){
    if(czyJestTrybNauki){
       qDebug("Tryb nauki");
       return;
    }
    else{
        qDebug("WczytajPrzyklad() - zaczynamy.");
        int wysokosc = imgLewy->height();
        int szerokosc = imgLewy->width();
        Przyklad p = Przyklad(-1);
        for(int kolumna = 0; kolumna < wysokosc; kolumna = kolumna + 10){
            for(int wiersz = 0; wiersz < szerokosc; wiersz = wiersz + 10){
                QRgb kolor = imgLewy->pixel(wiersz, kolumna);
                if(kolor == qRgb(255,255,255)){
//                    qDebug("Kolor biały");
                    p.dodajDoListy(-1);
                }
                if(kolor == qRgb(0,0,0)){
//                    qDebug("Kolor czarny");
                    p.dodajDoListy(1);
                }
            }
        }
        qDebug("Przeczytaliśmy caly obrazek.");
        sprawdzPrzyklad(p);
    }
    qDebug("Koniec metody wczytajPrzyklad().");
}




/**
 * Metoda ta jest akcją wykonywaną po naciśnięciu przycisku losujPrzykład.
 *  - losujemy przykład z listy,
 *  - zaburzamy go,
 *  - zaburzony przykład przepuszczamy przez sieć perceptronów,
 *  - wynik wyświetlamy na prawym rysunku,
 *
 * 4 grudnia 2018
 */
void Autoasocjator::losujPrzyklad(){
    if(czyJestTrybNauki){
       qDebug("Tryb nauki");
       return;
    }
    else{
        int numerWylosowanego = rand() % 3;
        qDebug("Losowanie przykladu do testów - wylosowano przyklad: %d", numerWylosowanego);
        Przyklad wylosowany = listaPrzykladow[numerWylosowanego];
        wylosowany = zaburzPrzyklad(wylosowany);
        qDebug("Rysujemy prawy rysunek");
        rysujRysunek(wylosowany);
        qDebug("Rysujemy lewy rysunek (po sprawdzeniu).");
        sprawdzPrzyklad(wylosowany);
    }
}

/**
 * Metoda ma za zadanie zapisac wagi do pliku.
 *
 * W wersji z 3 grudnia 2018 odpowiada za wylosowanie rysunku i narysowanie go na lewym oknie.
 */
void Autoasocjator::zapiszWagi(){
    qDebug("Rysujemy przykład.");
    int n = rand() % 3;
    Przyklad p = listaPrzykladow[n];
    qDebug("Przykład wylosowany: %d", n+1);
    rysujRysunekPrawy(p);
    qDebug("Narysowano wylosowany przykład.");
}

/**
 * Metoda ma za zadanie wczytać wagi z pliku.
 */
void Autoasocjator::wczytajWagi(){
    qDebug("Rysujemy przykład.");
    int n = rand() % 3;
    Przyklad p = listaPrzykladow[n];
    qDebug("Przykład wylosowany: %d", n+1);
    p = zaburzPrzyklad(p);
    rysujRysunek(p);
    qDebug("Narysowano wylosowany przykład.");
}

/**
 * Metoda ta kopiuje obraz prawy (będący wynikiem działania naszej sieci perceptronów),
 * żeby stał się naszym obrazkiem roboczym, którego możemy wczytać i przetworzyć jeszcze raz.
 *
 * 3 grudnia 2018
 * 4 grudnia 2018
 */
void Autoasocjator::potraktujJakoRoboczy(){
//    qDebug("metoda potraktujJakoRoboczy");
    *imgLewy = *imgPrawy;
    imgPrawy->fill(Qt::white);
    update();
//    qDebug("Koniec metody potraktujJakoRoboczy");
}
