#-------------------------------------------------
#
# Project created by QtCreator 2018-11-25T19:33:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 2018_WdSN_Autoasocjator
TEMPLATE = app

QMAKE_CXXFLAGS += -O2


SOURCES += main.cpp\
        autoasocjator.cpp \
    perceptron.cpp \
    przyklad.cpp

HEADERS  += autoasocjator.h \
    perceptron.h \
    przyklad.h

FORMS    += autoasocjator.ui
