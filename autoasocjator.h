#ifndef AUTOASOCJATOR_H
#define AUTOASOCJATOR_H

#include <QMainWindow>
#include <QPushButton>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QVector>
#include <przyklad.h>
#include <perceptron.h>

namespace Ui {
class Autoasocjator;
}

class Autoasocjator : public QMainWindow
{
    Q_OBJECT

public:
    explicit Autoasocjator(QWidget *parent = 0);
    ~Autoasocjator();

private:
    Ui::Autoasocjator *ui;

    QImage img1;
    QImage img2;
    QImage img3;
    QImage *imgLewy;
    QImage *imgPrawy;
    bool czyJestTrybNauki;

    QVector<Perceptron> listaPerceptronow;
    QVector<Przyklad> listaPrzykladow;

protected:
    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *event);

    void putPixel(int x, int y, QRgb color, QImage *obrazek);

    void zamalujSuperPixel(int x, int y);
    void zamalujSuperPixel(int x, int y, QRgb kolor);
    void rysujRysunek(Przyklad p);
    void rysujRysunekPrawy(Przyklad p);

    void sprawdzPrzyklad(Przyklad &p);

    Przyklad zaburzPrzyklad(Przyklad p);

    void czysc();

public slots:
    void uczPerceptrony();
    void wczytajPrzyklad();
    void losujPrzyklad();

    void zapiszWagi();
    void wczytajWagi();

    void potraktujJakoRoboczy();

};

#endif // AUTOASOCJATOR_H
