#include "przyklad.h"
#include <stdlib.h>
#include <stdio.h>
#include <QString>
#include <QVector>
#include <QImage>

/**
 * Na podstawie mojego kodu napisanego w javie oraz wczesniejszego zadania - Perceptronów.
 *
 * Rafał Kaczorkiewicz
 * 8 października 2018,
 * 4 listopada 2018,
 * 26 listopada 2018
 *
 * Zakładamy, że każdy obrazek możemy utożsamić z liczbą - wtedy możemy na tej płaszczyźnie
 * utożsamić rysunek z cyfrą.
 *
 * Od 19 stycznia 2019 obszar biały jest -1, obszar czarny jest 1.
 */

Przyklad::Przyklad(){
    lista = QVector<int>();
    jakaJestCyfra = -1;
}

Przyklad::Przyklad(int cyfra){
    lista = QVector<int>();
    jakaJestCyfra = cyfra;
}

void Przyklad::setJakaJestCyfra(int jakaJestCyfra){
    this->jakaJestCyfra = jakaJestCyfra;
}

int Przyklad::getJakaJestCyfra(){
    return jakaJestCyfra;
}

QVector<int> Przyklad::getLista(){
    return lista;
}

void Przyklad::setLista(QVector<int> lista){
    this->lista = lista;
}

int Przyklad::dlugoscPrzykladu(){
    return lista.size();
}

int Przyklad::getMiejsce(int miejsce){
    return lista.at(miejsce);
}

void Przyklad::setMiejse(int miejsce, int wartosc){
    lista.replace(miejsce, wartosc);
}

void Przyklad::dodajDoListy(int i){
    lista.push_back(i);
}

void Przyklad::wyczyscListeAbyStworzycKolejnyPrzyklad(){
    lista.clear();
}

/**
 * metoda wypisuje na ekranie dany przykład
 */
void Przyklad::wypisz(){
    for(int i = 0; i < lista.size(); i++){
        //qDebug(" %d ", lista.at(i));
        fprintf(stderr,"%c", lista.at(i) == 1 ? 'X' : '-');
        if(i % 50 == 49 ){
            fprintf(stderr,"\n");
        }
    }
}

/**
 * Metoda bierze jako parametr string oraz inta oznaczającego, jaką cyfrę przedstawia napis.
 * Zwraca tablicę intów.
 */
Przyklad Przyklad::dodajDoListy(QString napis, int cyfra){
//    qDebug("Zaczynamy dodawać przykład do listy.");
    Przyklad wynik = Przyklad(cyfra);

    for(int i = 0; i < napis.size() ; i++){
        int l;
        // potencjalnie poniżej jest słaby punkt, odejmujemy 48 jako przesunięcie w unicode
        l = (int) napis.at(i).unicode() - 48;
        //qDebug("%d", l);
        lista.push_back(l);
    }
//    qDebug("Dodaliśmy przykład do listy");
    return wynik;
}

/**
 * 26 listopada 2018
 * Metoda jako parametr przyjmuje obraz, a następnie czytając go piksel po pikselu
 * tworzy z niego przykład. Jeżeli piksel jest czarny, to 1, (zapalony), jeżeli biały - to 0.
 *  Zwraca Przykład.
 */
Przyklad Przyklad::wczytajObraz(QImage obraz){
    qDebug("WczytajObraz - zaczynamy.");
    int wysokosc = obraz.height();
    int szerokosc = obraz.width();
    Przyklad *p = new Przyklad();

    qDebug("Zmienne zainicjowane, zaczynamy czytanie.");
    for(int wiersz = 0; wiersz < wysokosc; wiersz++){
        for(int kolumna = 0; kolumna < szerokosc; kolumna++){
            QRgb kolor = obraz.pixel(kolumna, wiersz);
//            QRgb kolor = obraz.pixel(wiersz, kolumna);
            // czy kolor jest biały, czy czarny?
            if(kolor == qRgb(255,255,255)){
//                qDebug("Kolor biały");
                p->dodajDoListy(-1);
            }
            if(kolor == qRgb(0,0,0)){
//                qDebug("Kolor czarny");
                p->dodajDoListy(1);
            }
        }
    }
    qDebug("Przeczytaliśmy cały obrazek.");

    setLista(p->getLista());

    return *p;
}
