#ifndef PERCEPTRON_H
#define PERCEPTRON_H

#include "przyklad.h"
#include <QVector>

class Perceptron
{
public:
    Perceptron();
    Perceptron(int liczba);
    void setN(int n);
    int getN();
    void setTheta(double theta);
    double getTheta();
    int getErr();
    void setErr(int ERR);
    void wartoscERR(double T_j);
    void setStalaUczenia(double stalaUczenia);
    double getStalaUczenia();
    void setCzyPrzykladJestTaLiczba(int i);
    int getCzyPrzykladJestTaLiczba();
    void setWynikDzialaniaSieci(int wynikDzialaniaSieci);
    int getWynikDzialaniaSieci();
    void setTablicaWag(QVector<double> tablicaWag);
    QVector<double> getTablicaWag();

    int coJestNaWyjsciu(Przyklad wektor);

    // poniższa metoda robi wszystko sama
    void wszystkieOperacje2(Przyklad wektor);

    void uaktualnijWagi(Przyklad pp);
    void wypiszTabliceWag();

    int liczKlasyfikowanePrzyklady(QVector<Przyklad> lista);

private:
    QVector<double> tablicaWag;
    int n;                          // cyfra, jaką rozpoznaje perceptron
    double theta;
    int ERR;
    int wynikDzialaniaSieci;
    double stalaUczenia;
    int czyPrzykladJestTaLiczba;    // -1 - nie jest, 1 - jest

};

#endif // PERCEPTRON_H
