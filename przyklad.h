#ifndef PRZYKLAD_H
#define PRZYKLAD_H

#include <QVector>
#include <iostream>
#include <QString>
#include <QImage>


class Przyklad
{
public:
    Przyklad();
    Przyklad(int cyfra);

    void setJakaJestCyfra(int jakaJestCyfra);
    int getJakaJestCyfra();
    QVector<int> getLista();
    void setLista(QVector<int> lista);

    int dlugoscPrzykladu();
    int getMiejsce(int miejsce);
    void setMiejse(int miejsce, int wartosc);

    void dodajDoListy(int i);

    void wyczyscListeAbyStworzycKolejnyPrzyklad();

    void wypisz();

    Przyklad dodajDoListy(QString napis, int cyfra);

    Przyklad wczytajObraz(QImage obraz);


private:
    int jakaJestCyfra;
    QVector<int> lista;
};

#endif // PRZYKLAD_H
