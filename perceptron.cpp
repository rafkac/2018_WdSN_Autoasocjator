#include "perceptron.h"
#include "przyklad.h"

/**
 * @author RafKac
 * 8 października 2018
 * 4 listopada 2018
 * 25 listopada 2018
 *
 * Klasa ta jest pojedyńczym perceptronem.
 *
 * Jest to mój kod z pierwszego zadania.
 *
 * */

Perceptron::Perceptron(){
//    srand(time(NULL));
    tablicaWag = QVector<double>();
    for(int i = 0; i < 2500; i++){
        double waga = 2.0 * ((double)rand()/ RAND_MAX) - 1;
        tablicaWag.push_back(waga);
    }
    this->n = 0;
    this->theta = 2.0 * ((double)rand()/ RAND_MAX) - 1;
    this->ERR = 0;
    this->wynikDzialaniaSieci = 0;
    this->stalaUczenia = 0.05;
    this->czyPrzykladJestTaLiczba = 0;
}

Perceptron::Perceptron(int liczba){
//    srand(time(NULL));
    tablicaWag = QVector<double>();
    for(int i = 0; i < 2500; i++){
        double waga = 2.0 * ((double)rand()/ RAND_MAX) - 1;
        tablicaWag.push_back(waga);
//        qDebug("waga %d: %f", i, waga);
    }
    this->n = liczba;
    this->theta = 2.0 * ((double)rand()/ RAND_MAX) - 1;
    this->ERR = 0;
    this->wynikDzialaniaSieci = 0;
    this->stalaUczenia = 0.05;
    this->czyPrzykladJestTaLiczba = 0;
}

void Perceptron::setN(int n){
    this->n = n;
}

int Perceptron::getN(){
    return n;
}

void Perceptron::setTheta(double theta){
    this->theta = theta;
}

double Perceptron::getTheta(){
 //   qDebug("metoda getTheta()");
    return theta;
}

int Perceptron::getErr(){
    return ERR;
}

void Perceptron::setErr(int ERR){
    this->ERR = ERR;
}

void Perceptron::wartoscERR(double T_j){
    double pomoc;
    if(T_j == n){
        pomoc = 1;
    }
    else{
        pomoc = -1;
    }
    ERR = pomoc - wynikDzialaniaSieci;
}

void Perceptron::setStalaUczenia(double stalaUczenia){
    this->stalaUczenia = stalaUczenia;
}

double Perceptron::getStalaUczenia(){
    return stalaUczenia;
}

void Perceptron::setCzyPrzykladJestTaLiczba(int i){
    czyPrzykladJestTaLiczba = i;
}

int Perceptron::getCzyPrzykladJestTaLiczba(){
    return czyPrzykladJestTaLiczba;
}

void Perceptron::setWynikDzialaniaSieci(int wynikDzialaniaSieci){
    this->wynikDzialaniaSieci = wynikDzialaniaSieci;
}

int Perceptron::getWynikDzialaniaSieci(){
    return wynikDzialaniaSieci;
}

void Perceptron::setTablicaWag(QVector<double> tablicaWag){
    this->tablicaWag = tablicaWag;
}

QVector<double> Perceptron::getTablicaWag(){
    return tablicaWag;
}

int Perceptron::coJestNaWyjsciu(Przyklad wektor){
    double suma = -1 * theta;
//    qDebug("Rozmiar tablicy wag to: %d", tablicaWag.size());
    for(int i = 0; i < tablicaWag.size(); i++){
        suma += tablicaWag.at(i) * wektor.getMiejsce(i);
    }
    if(suma >= 0){
        wynikDzialaniaSieci = 1;
    }
    else{
        wynikDzialaniaSieci = -1;
    }
    return (int) wynikDzialaniaSieci;
}



/**
 * Metoda powyższa, ale w bardziej estetycznej postaci.
 * Robi dokładnie to samo, ale nie ma zakomentowanych linii powstałych we wcześniejszych etapach pracy twórczej.
 *
 * Jest to zmodyfikowana wersja metody z zadania pierwszego, albowiem porównujemy, czy na i-tym pikselu otrzymaliśmy to,
 * co powinniśmy, czy też nie.
 *
 * 4 grudnia 2018
 *
 */
void Perceptron::wszystkieOperacje2(Przyklad wektor){
    double suma = -1 * this->getTheta();
    int czyToTaCyfra = 0;

    for(int i = 0; i < tablicaWag.size(); i++){
        suma = suma + (double)tablicaWag.at(i) * (double)wektor.getMiejsce(i);
    }
    if(suma >= 0.0){
        setWynikDzialaniaSieci(1);
    }
    else{
        setWynikDzialaniaSieci(-1);
    }
    int coMamyNaItymPikselu = wektor.getMiejsce(getN());
    int t = coMamyNaItymPikselu == -1 ? -1 : 1 ;
    /**
     * Jeśli na i-tym pikselu mamy jedynkę i suma jest dodatnia (czyli rozpoznaliśmy jedynkę),
     * lub jeśli mamy zero i suma jest ujemna (czyli rozpoznaliśmy zero), to ok.
     *
     * Dla 0, jeśli suma jest dodatnia, to błąd.
     * Analogicznie dla 1, jeśli suma jest ujemna.
     */
    if((coMamyNaItymPikselu == 1 && wynikDzialaniaSieci > 0) || (coMamyNaItymPikselu == -1 && wynikDzialaniaSieci < 0)){
        czyToTaCyfra = 1;
    }
    else{
        czyToTaCyfra = -1;
    }
    setCzyPrzykladJestTaLiczba(czyToTaCyfra);
    setErr(t - wynikDzialaniaSieci);
//    setErr(czyToTaCyfra - getWynikDzialaniaSieci());
//    qDebug("Rozpatrywana: %d, Czy to ta cyfra? %d, ERR: %d, wynik działania sieci: %d",cyfraJakaJestPrzyklad,czyToTaCyfra, getErr(), getWynikDzialaniaSieci());
}


/**
 * Metoda odpowiada za wypisanie tablicy wag.
 */
void Perceptron::wypiszTabliceWag(){
    qDebug("Rozmiar tablicy wag: %d", tablicaWag.size());
    for(int i = 0; i < tablicaWag.size(); i++){
        qDebug("%f", tablicaWag.at(i));
    }
}

/**
 * Poprawna metoda służąca uaktualnianiu wag - ta wcześniejsza jest błędna, bo pracuje na kopiach danych,
 * zatem nie uaktualnia ich po wykonaniu operacji.
 *
 * 10 listopada 2018
 *
 * 17 grudnia - obserwacja - ta metoda na pewno jest wykonywana, bo idą logi
 */
void Perceptron::uaktualnijWagi(Przyklad pp){
//    qDebug("Uaktualniamy wagi.");
//    QVector<double> localTablicaWag = getTablicaWag();
//    int iloscJedynek = 0;
//    int iloscZer = 0;
    for(int i=0; i < tablicaWag.size(); i++){
//        double wpis = tablicaWag.at(i);
        double wpis = tablicaWag[i];
//        if(pp.getMiejsce(i) == 1){
//            qDebug("Przed zmianą: %f, err: %d, mamy na miejscu: %d", wpis, getErr(), pp.getMiejsce(i));
//            iloscJedynek++;
//        }
//        if(pp.getMiejsce(i) == 0){
//            iloscZer++;
//        }
        wpis = wpis + getStalaUczenia() * (double)getErr() * (double) pp.getMiejsce(i);
        tablicaWag[i]=wpis;
//        if(pp.getMiejsce(i) != 0){
//            qDebug("Po zmianie: %f", wpis);
//        }
    }
//    qDebug("Zmiana wartości theta: %f, ERR: %d, stalaUczenia: %f", this->getTheta(), this->getErr(), this->getStalaUczenia());
    double t = getTheta() - (double)getErr() * (double)getStalaUczenia();
    qDebug("Stara theta: %f, nowa: %f", getTheta(), t);
    setTheta(t);
//    qDebug("Nowa theta: %f", this->getTheta());
//    setTablicaWag(localTablicaWag);
//    if(iloscJedynek > 0){
//        qDebug("mieliśmy %d jedynek, zer: %d, nr przykładu: %d", iloscJedynek, iloscZer, pp.getJakaJestCyfra());
//    }
//    else{
//        qDebug("Zero niezerowych pikseli w przekazanym do testów rysunku.");
//bv    }
}


/**
 * Metoda zlicza prawidłowo klasyfikowane przez dany perceptron przykłady (z tych niezaburzonych).
 */
int Perceptron::liczKlasyfikowanePrzyklady(QVector<Przyklad> lista){
//    qDebug("Liczymy klasyfikowane przykłady.");
    int wynik = 0;

    for(int i = 0; i < lista.size(); i++){
        Przyklad p = lista[i];
        this->wszystkieOperacje2(p);
        if(this->getErr() == 0){
            wynik++;
        }
    }

    return wynik;
}













